# SMS interface for managing your account.
A bank wants to allow customers to perform simple operations on their account
using their mobile phones. The technical sending and receiving of the SMS itself
will be done by another team. Your task is to handle the incoming and outgoing
strings and perform all necessary operations. Some back-end services are already
available to support your task and are available to you as interfaces.

## Example Requests

|SMS Command|	Example response|	Notes|
| -----------|---------------------|-----------|
|BALANCE|	1500|	Returns your balance in EUR
|SEND-100-FFRITZ	|OK|	Send 100 EUR to user with username FFRITZ
|SEND-100-FFRITZ	| ERR – INSUFFICIENT FUNDS|	User hasn’t got enough money to make the transfer
|SEND-100-FFRITZ	| ERR – NO USER|	System can’t find the user with username FFRITZ
|TOTAL-SENT-FFRITZ	| 560|	Get the total amount sent so far to user FFRITZ
|TOTAL-SENT-FFRITZ-MSMITH	| 560,250|	Get the total amounts sent to users FFRITZ and MSMITH as a comma separated list.
|TOTAL-SENT-FFRITZ-MSMITH	| ERR – NO USER |	Either the system cannot find the user FFRITZ or the system cannot find MSMITH
|XYZ	|ERR – UNKNOWN COMMAND|	The command was not recognized.

## Implementation details
The back-end systems that are available are:

```
public interface UserManager {
	boolean existsUser(String username);
	BigDecimal getBalance(String username);
	String getUserNameForDeviceId(String deviceId);
}
public interface TransferManager {
	void sendMoney(String senderUsername, String recipientUsername, BigDecimal amount)
	List<BigDecimal> getAllTransactions(String senderUsername, String recipientUsername); 
}
```

The module you are implementing should have the following interface entry point:

```
public interface SMSHandler {
	/**
     * @param smsContent the incoming SMS command string.
     * @param senderDeviceId is a unique string that uniquely identifies the customer’s mobile device. The UserManager proves a means to identify the sender user.
     * @return The SMS content that should be returned to the user.
     */
    String handleSmsRequest(String smsContent, String senderDeviceId);
}
```

## Requirements

1. The command codes will behave as described in the above table.
2. The module will be extended in the future with new command types. Ensure that the solution is easily extended.
3. Adequate logging should be performed to allow operations staff to monitor the system once it goes into production.
4. The implementation should be well tested.
5. The implementation should scale to a large number of users.
 