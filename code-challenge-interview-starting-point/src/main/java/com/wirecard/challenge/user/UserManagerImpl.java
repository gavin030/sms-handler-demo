package com.wirecard.challenge.user;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;

public class UserManagerImpl implements UserManager {

    private final JdbcOperations jdbcOperations;

    public UserManagerImpl(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public boolean userExists(String username) {
        if (StringUtils.isEmpty(username)) {
            return false;
        }
        return jdbcOperations.queryForObject("select count(1) from users u where lower(u.name)=?", new Object[]{username.toLowerCase()}, Long.class) > 0L;
    }

    @Override
    public BigDecimal getBalance(String username) {
        return jdbcOperations.queryForObject("select balance from users u where u.name=?", new Object[]{username}, BigDecimal.class);
    }

    @Override
    public String getUserNameForDeviceId(String deviceId) {
        return jdbcOperations.queryForObject("select name from users u where u.device_id=?", new Object[]{deviceId}, String.class);
    }
}
