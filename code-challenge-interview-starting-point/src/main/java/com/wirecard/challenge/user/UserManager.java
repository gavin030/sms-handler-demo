package com.wirecard.challenge.user;

import java.math.BigDecimal;

public interface UserManager {
    boolean userExists(String username);
    BigDecimal getBalance(String username);
    String getUserNameForDeviceId(String deviceId);
}
