package com.wirecard.challenge;

import com.wirecard.challenge.sms.SmsHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(scanBasePackageClasses = {SmsApplication.class})
public class SmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsApplication.class, args);
    }

    @RestController
    private class SmsControllerImpl {
        private final SmsHandler smsHandler;

        public SmsControllerImpl(SmsHandler smsHandler) {
            this.smsHandler = smsHandler;
        }

        @GetMapping("/sms-api/{deviceId}/{command}")
        public @ResponseBody
        String executeCommand(@PathVariable("deviceId") String deviceId, @PathVariable("command") String command) {
            return smsHandler.handleSmsRequest(command, deviceId);
        }
    }

}
