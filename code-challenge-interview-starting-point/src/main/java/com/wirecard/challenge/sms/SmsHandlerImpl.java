package com.wirecard.challenge.sms;

import org.springframework.beans.factory.annotation.Autowired;

import com.wirecard.challenge.command.Command;
import com.wirecard.challenge.command.CommandUtils;
import com.wirecard.challenge.transfer.TransferManager;
import com.wirecard.challenge.user.UserManager;

public class SmsHandlerImpl implements SmsHandler {
	@Autowired
	private UserManager userManager;
	@Autowired
	private TransferManager transferManager;
	private CommandUtils commandUtils;
	
	public SmsHandlerImpl() {
		commandUtils = new CommandUtils(transferManager, userManager);
	}

    @Override
    public String handleSmsRequest(String smsContent, String senderDeviceId) {
    	String senderName = userManager.getUserNameForDeviceId(senderDeviceId);
        Command command = commandUtils.parseCommandString(senderName, smsContent);
        return command.execute();
    }


}
