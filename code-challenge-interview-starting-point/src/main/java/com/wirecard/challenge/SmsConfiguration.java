package com.wirecard.challenge;

import com.wirecard.challenge.sms.SmsHandlerImpl;
import com.wirecard.challenge.transfer.TransferManagerImpl;
import com.wirecard.challenge.user.UserManagerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class SmsConfiguration {
    private final DataSource dataSource;

    public SmsConfiguration(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public UserManagerImpl userManager() {
        return new UserManagerImpl(new JdbcTemplate(dataSource));
    }

    @Bean
    public TransferManagerImpl transferManager() {
        return new TransferManagerImpl(new JdbcTemplate(dataSource));
    }


    @Bean
    public SmsHandlerImpl smsHandler() {
        return new SmsHandlerImpl();
    }


}
