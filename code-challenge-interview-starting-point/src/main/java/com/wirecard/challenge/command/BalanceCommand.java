package com.wirecard.challenge.command;

import com.wirecard.challenge.user.UserManager;

public class BalanceCommand implements Command {
	private String senderName;
	private UserManager userManager;
	
	public BalanceCommand(String senderName, UserManager userManager) {
		this.senderName = senderName;
		this.userManager = userManager;
	}

	@Override
	public String execute() {
		return userManager.getBalance(senderName).toString();
	}

}
