package com.wirecard.challenge.command;

public interface Command {
	public String execute();
}
