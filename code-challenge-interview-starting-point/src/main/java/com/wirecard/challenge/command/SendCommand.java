package com.wirecard.challenge.command;

import java.math.BigDecimal;

import com.wirecard.challenge.transfer.TransferManager;
import com.wirecard.challenge.user.UserManager;

public class SendCommand implements Command {
	public static final String TRANSACTION_SUCCESS_MESSAGE = "OK";
	public static final String INSUFFICIENT_FUND_ERR_MESSAGE = "ERR �C INSUFFICIENT FUNDS";
	public static final String RECEIVER_NOT_EXIST_ERR_MESSAGE = "ERR �C NO USER";
	
	private String senderName;
	private BigDecimal amount;
	private String receiverName;
	private TransferManager transferManager;
	private UserManager userManager;
	
	public SendCommand(String senderName, BigDecimal amount, String receiverName, TransferManager transferManager, UserManager userManager) {
		this.senderName = senderName;
		this.amount = amount;
		this.receiverName = receiverName;
		this.transferManager = transferManager;
		this.userManager = userManager;
	}

	@Override
	public String execute() {
		if(userManager.getBalance(senderName).compareTo(amount) < 0) return INSUFFICIENT_FUND_ERR_MESSAGE;
		if(!userManager.userExists(receiverName)) return RECEIVER_NOT_EXIST_ERR_MESSAGE;
		transferManager.sendMoney(senderName, receiverName, amount);
		return TRANSACTION_SUCCESS_MESSAGE;
	}

}
