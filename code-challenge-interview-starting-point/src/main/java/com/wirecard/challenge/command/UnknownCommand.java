package com.wirecard.challenge.command;

public class UnknownCommand implements Command {
	public static final String ERR_MESSAGE = "ERR �C UNKNOWN COMMAND";
	
	public UnknownCommand() {}

	@Override
	public String execute() {
		return ERR_MESSAGE;
	}

}
