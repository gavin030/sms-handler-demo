package com.wirecard.challenge.command;

import java.math.BigDecimal;
import java.util.List;

import com.wirecard.challenge.transfer.TransferManager;
import com.wirecard.challenge.user.UserManager;

public class TotalCommand implements Command {
	public static final String RECEIVER_NOT_EXIST_ERR_MESSAGE = "ERR �C NO USER";
	
	private String senderName;
	private List<String> receivers;
	private TransferManager transferManager;
	private UserManager userManager;
	
	public TotalCommand(String senderName, List<String> receivers, TransferManager transferManager, UserManager userManager) {
		this.senderName = senderName;
		this.receivers = receivers;
		this.transferManager = transferManager;
		this.userManager = userManager;
	}

	@Override
	public String execute() {
		StringBuilder buff = new StringBuilder();
		List<BigDecimal> allTransactions;
		BigDecimal totalAmount;
		
		for(String receiver : receivers) {
			if(!userManager.userExists(receiver)) return RECEIVER_NOT_EXIST_ERR_MESSAGE;
			allTransactions = transferManager.getAllTransactions(senderName, receiver);
			totalAmount = new BigDecimal("0.0");
			for(BigDecimal amount : allTransactions) {
				totalAmount = totalAmount.add(amount);
			}
			buff.append(totalAmount.toString());
			buff.append(',');
		}
		buff.deleteCharAt(buff.length() - 1);
		
		return buff.toString();
	}

}
