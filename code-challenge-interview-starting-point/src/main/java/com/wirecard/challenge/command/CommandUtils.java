package com.wirecard.challenge.command;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.wirecard.challenge.transfer.TransferManager;
import com.wirecard.challenge.user.UserManager;

public class CommandUtils {
	private TransferManager transferManager;
	private UserManager userManager;
	
	public CommandUtils(TransferManager transferManager, UserManager userManager) {
		this.transferManager = transferManager;
		this.userManager = userManager;
	}
	
	public Command parseCommandString(String senderName, String commandString) {
		if(commandString.equals("BALANCE")) return new BalanceCommand(senderName, userManager);
		else if(commandString.startsWith("SEND")) {
			String[] params = commandString.split("-");
			return new SendCommand(senderName, new BigDecimal(params[1]), params[2], transferManager, userManager);
		}else if(commandString.startsWith("TOTAL-SENT")) {
			String[] params = commandString.split("-");
			List<String> receivers = new ArrayList<String>();
			for(int i = 2; i < params.length; i++) {
				receivers.add(params[i]);
			}
			return new TotalCommand(senderName, receivers, transferManager, userManager);
		}else return new UnknownCommand();
	}
}
