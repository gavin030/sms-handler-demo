package com.wirecard.challenge.transfer;

import org.springframework.jdbc.core.JdbcOperations;

import java.math.BigDecimal;
import java.util.List;

public class TransferManagerImpl implements TransferManager {

    private final JdbcOperations jdbcOperations;

    public TransferManagerImpl(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public void sendMoney(String senderUsername, String recipientUsername, BigDecimal amount) {
        jdbcOperations.update("insert into transfers (from_user, to_user, amount) values (?, ?, ?)", new Object[]{senderUsername, recipientUsername, amount});
        jdbcOperations.update("update users u set u.balance = u.balance - ? where u.name = ?", new Object[]{amount, senderUsername});
        jdbcOperations.update("update users u set u.balance = u.balance + ? where u.name = ?", new Object[]{amount, recipientUsername});
    }

    @Override
    public List<BigDecimal> getAllTransactions(String senderUsername, String recipientUsername) {
        return jdbcOperations.query("select amount from transfers t where lower(t.from_user) = ? and lower(t.to_user) = ?", new Object[]{senderUsername.toLowerCase(), recipientUsername.toLowerCase()}, (rs, rowNum) -> rs.getBigDecimal(1));
    }
}
